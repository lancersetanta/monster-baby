﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum FingerTouchType
{
	Left,
	Right,
	Up,
	Down
}

public class CheckSlide : MonoBehaviour {
	
	Vector2 screenPos = new Vector2();
	public GameObject FireMonster_0;
	public GameObject FireMonster_1;
	
	void Start()
    {
        
    }

	void Update()
    {
		#if UNITY_EDITOR || UNITY_STANDALONE
			MouseInput();
		#elif UNITY_ANDROID
			MobileInput();
		#endif
	}
	
	void MouseInput()
	{
		if (Input.GetMouseButtonDown(0))
		{
			screenPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		}

		if(Input.GetMouseButtonUp(0))
		{
			Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

			FingerTouchType fingerPos = HandDirection(screenPos, pos);
		}
	}
	
	FingerTouchType HandDirection(Vector2 StartPos, Vector2 EndPos)
	{
		FingerTouchType mDirection;

		if (Mathf.Abs (StartPos.x - EndPos.x) > Mathf.Abs (StartPos.y - EndPos.y)) {
			if (StartPos.x > EndPos.x) {
				mDirection = FingerTouchType.Left;
				print("Please slide down.");
			} else {
				mDirection = FingerTouchType.Right;
				print("Please slide down.");
			}
		} else {
			if (screenPos.y > EndPos.y) {
				mDirection = FingerTouchType.Down;
				Instantiate(FireMonster_0, new Vector3(4.13f, 2.85f, -1f), Quaternion.identity);
				Instantiate(FireMonster_1, new Vector3(6.41f, -0.95f, -1f), Quaternion.identity);
				Destroy (gameObject);
			} else {
				mDirection = FingerTouchType.Up;
				print("Please slide down.");
			}
		}
		return mDirection;
	}
}