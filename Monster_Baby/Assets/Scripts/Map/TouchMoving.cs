﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TouchMoving : MonoBehaviour 
{
	public int LoadingSceneIndex;
	
	void OnMouseDown()
	{
		SceneManager.LoadScene(LoadingSceneIndex);
	}
}