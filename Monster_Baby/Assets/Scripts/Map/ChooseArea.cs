﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChooseArea : MonoBehaviour 
{
    public GameObject FireArea;
	public GameObject WaterArea;
	public GameObject GrassArea;
	
	static GameObject currentArea = null;
    
    void Start()
    {
        /*GetComponent<Button>().onClick.AddListener(() => {
            ClickEvent ();
        });*/
		
		GameObject.Find("Dropdown").GetComponent<Dropdown>().onValueChanged.AddListener(ShowArea);
    }
    
    /*void ClickEvent()
    {
        Instantiate(FireArea, Vector2.zero, Quaternion.identity);
    }*/
	
	public void ShowArea(int value)
    {
		if (currentArea != null)
            Destroy(currentArea);
			
        switch (value)
        {
            case 1:
                currentArea = (Instantiate(FireArea, new Vector3(-0.22f, 1.57f, -1f), Quaternion.identity));
                break;
            case 2:
                currentArea = (Instantiate(WaterArea, new Vector3(-6f, 1.88f, -1f), Quaternion.identity));
                break;
            case 3:
                currentArea = (Instantiate(GrassArea, new Vector3(6.18f, 2.04f, -1f), Quaternion.identity));
                break;
        }
    }
}