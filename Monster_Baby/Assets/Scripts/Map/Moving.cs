﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Moving : MonoBehaviour 
{
	public int LoadingSceneIndex;
	
    void OnTriggerEnter2D(Collider2D col)
    {
        SceneManager.LoadScene(LoadingSceneIndex);
    }
}