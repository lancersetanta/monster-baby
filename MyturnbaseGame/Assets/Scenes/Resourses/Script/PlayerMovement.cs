﻿using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using UnityEngine;



// void Start () { 暫時不會用到先註解 Use this for initialization

public class PlayerMovement : MonoBehaviour
{

	//宣告兩個浮點數字(floating point)分別設定腳色的X移動速度跟Y移動速度
	

	public float movementSpeedX;
	public float movementSpeedY;

	//宣告Unity的Animator以執行腳色動畫
	public Animator playerAni;

	//宣告SpriteRenderer控制腳色轉向
	public SpriteRenderer playerScale;


	// Update is called once per frame
	void Update()
	{



		//移動的距離 X
		float movementX = Input.GetAxis("Horizontal") * movementSpeedX * Time.deltaTime;
		bool IsWalking = false;

		//判斷腳色行走時則保持行走動畫,反之回復待機動畫
		if (IsWalking)
		{
			if (playerAni.GetInteger("advenced") == 0)
			{
				playerAni.SetInteger("advenced", 1);
			}
		}

		else
		{
			if (playerAni.GetInteger("advenced") == 1)
			{
				playerAni.SetInteger("advenced", 0);
			}
		}

		//移動的距離 Y
		float movementY = Input.GetAxis("Vertical") * movementSpeedY * Time.deltaTime;

		//新的座標
		Vector3 newPos = new Vector3(transform.position.x + movementX, transform.position.y + movementY, transform.position.z);

		//把座標設為新的座標
		transform.position = newPos;

		//如果移動X或Y向量>0，腳色執行移動動畫
		if ((movementX > 0 || movementY > 0))
		{
			IsWalking = true;
			playerAni.SetInteger("advenced", 1);
			//UnityEngine.Debug.Log(movementX);

			//利用flip功能控制腳色轉向
			if (playerScale.flipX == true && movementX > 0)
			{
				playerScale.flipX = false;
			}
		}

		//如果移動X或Y向量<0，腳色執行移動動畫
		else if ((movementX < 0 || movementY < 0))
		{
			IsWalking = true;
			playerAni.SetInteger("advenced", 1);

			//利用flip功能控制腳色轉向
			if (playerScale.flipX == false && movementX < 0)
			{
				playerScale.flipX = true;
			}

		}


	}	


}
